<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Test_Action_Reject</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Test Action - Reject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Test_Fiedl_Update</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Test Fiedl Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Test WF RUle</fullName>
        <active>true</active>
        <formula>PRIORVALUE(Status__c) = &apos;Approved&apos; &amp;&amp;  ISPICKVAL(Status__c, &apos;Rejected&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
