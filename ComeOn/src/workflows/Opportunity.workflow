<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>TaskToMe_OppGreaterthan600</fullName>
        <actions>
            <name>New_Opportunity_with_Amount_600</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterThan</operation>
            <value>600</value>
        </criteriaItems>
        <description>If the amount &gt; 600.000 euro, a task will be created and assigned to Patrick Van Poeyer</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>New_Opportunity_with_Amount_600</fullName>
        <assignedTo>vignesh_acc2@sf.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Look out for this opportunity. Once in a life time..</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Opportunity with Amount &gt; 600$</subject>
    </tasks>
</Workflow>
