trigger subProjectTrigger on Sub_Project__c (before insert) {

    public Set<Id> subProjId = new Set<Id>();
    public List<Sub_Project__c> lstSubProjs = new List<Sub_Project__c>();
    public String lastSubProjName;
    public Integer lastSubProjNumber;
    
    if(trigger.isBefore && trigger.isinsert){
    
        lstSubProjs = [select id,name from Sub_Project__c order by createdDate desc limit 1];
        
        for(Sub_Project__c subProj: Trigger.New){
            subProjId.add(subProj.Id);
            
            if(lstSubProjs.size()>0)
            {
                for(Sub_Project__c obj: lstSubProjs)
                {
                    system.debug('ids equal??!!!!!!!'+(obj.Id != subProj.Id));                    
                    if(obj.Id != subProj.Id)
                    {
                        Integer length = obj.name.length();
                        system.debug('length!!!!'+obj.Name+'!!!'+length);
                        lastSubProjName = obj.name.substring((length-2),length);
                        system.debug('After substring!!!!!'+lastSubProjName);
                        if(lastSubProjName.isNumeric())
                        {
                            lastSubProjNumber = Integer.valueOf(lastSubProjName);                            
                            system.debug('numeric!!!!!!!'+lastSubProjNumber);
                        }                        
                    }                                        
                }
                if(lastSubProjNumber != null)
                {
                    if(lastSubProjNumber.format().Substring(0,1) == '0' && lastSubProjNumber.format().substring(0,2) == '9')
                    {
                        subProj.name = subProj.name+(lastSubProjNumber+1);
                    }
                    else if(lastSubProjNumber.format().Substring(0,1) == '0' && Integer.valueOf(lastSubProjNumber.format().Substring(0,1)) < 9)
                    {
                        lastSubProjNumber = lastSubProjNumber+1;
                        subProj.Name = subProj.Name+'0'+String.valueOf(lastSubProjNumber);
                    }
                    else
                    {
                        subProj.name = subProj.name+(lastSubProjNumber+1);
                    }
                    system.debug('final name!!!!!!'+subProj.Name);
                }
                else
                {
                    subProj.name = subProj.name+'01';
                }        
            }        
        } 
    }      
}