@isTest
private class CaptchaGeneratorTest{
    static testmethod void runTests() {
        Test.startTest();
        Captcha__c cap = new Captcha__c();
        cap.Name = 'ImageAttach';
        insert cap;
        
        Attachment atch=new Attachment(Name='A.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch1=new Attachment(Name='B.jpg',body=Blob.valueOf('Unit Test Attachment Body'),ParentId=cap.Id );
        Attachment atch2=new Attachment(Name='C.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch3=new Attachment(Name='D.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch4=new Attachment(Name='E.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch5=new Attachment(Name='F.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch6=new Attachment(Name='G.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch7=new Attachment(Name='H.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch8=new Attachment(Name='I.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch9=new Attachment(Name='J.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch10=new Attachment(Name='K.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch11=new Attachment(Name='L.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch12=new Attachment(Name='M.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch13=new Attachment(Name='N.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch14=new Attachment(Name='O.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch15=new Attachment(Name='P.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch16=new Attachment(Name='Q.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch17=new Attachment(Name='R.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch18=new Attachment(Name='S.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch19=new Attachment(Name='T.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch20=new Attachment(Name='U.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch21=new Attachment(Name='V.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch22=new Attachment(Name='W.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch23=new Attachment(Name='X.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch24=new Attachment(Name='Y.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch25=new Attachment(Name='Z.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch26=new Attachment(Name='X1a.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch27=new Attachment(Name='X1b.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch28=new Attachment(Name='X1c.jpg',body=Blob.valueOf('Unit Test Attachment Body'),ParentId=cap.Id );
        Attachment atch29=new Attachment(Name='X1d.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch30=new Attachment(Name='X1e.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch31=new Attachment(Name='X1f.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch32=new Attachment(Name='X1g.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch33=new Attachment(Name='X1h.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch34=new Attachment(Name='X1i.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch35=new Attachment(Name='X1j.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch36=new Attachment(Name='X1k.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch37=new Attachment(Name='X1l.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch38=new Attachment(Name='X1m.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch39=new Attachment(Name='X1n.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch40=new Attachment(Name='X1o.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch41=new Attachment(Name='X1p.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch42=new Attachment(Name='X1q.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch43=new Attachment(Name='X1r.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch44=new Attachment(Name='X1s.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch45=new Attachment(Name='X1t.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch46=new Attachment(Name='X1u.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch47=new Attachment(Name='X1v.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch48=new Attachment(Name='X1w.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch49=new Attachment(Name='X1x.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch50=new Attachment(Name='X1y.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch51=new Attachment(Name='X1z.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch52=new Attachment(Name='X10.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch53=new Attachment(Name='X11.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch54=new Attachment(Name='X12.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch55=new Attachment(Name='X13.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch56=new Attachment(Name='X14.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch57=new Attachment(Name='X15.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch58=new Attachment(Name='X16.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch59=new Attachment(Name='X17.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch60=new Attachment(Name='X18.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment atch61=new Attachment(Name='X19.jpg',body=Blob.valueOf('Unit Test Attachment Body') ,ParentId=cap.Id);
        Attachment [] dups = new Attachment [] {atch, atch1, atch2,atch3,atch4,atch5,atch6,atch7,atch8,atch9,atch10,
        atch11,atch12,atch13,atch14,atch15,atch16,atch17,atch18,atch19,atch20,
        atch21, atch22,atch23,atch24,atch25,atch26,atch27,atch28,atch29,atch30,
        atch31, atch32,atch33,atch34,atch35,atch36,atch37,atch38,atch39,atch40,
        atch41, atch42,atch43,atch44,atch45,atch46,atch47,atch48,atch49,atch50,
        atch51, atch52,atch53,atch54,atch55,atch56,atch57,atch58,atch59,atch60,atch61};
        insert dups;
        
        CaptchaGenerator cg = new CaptchaGenerator();
        //Valid CAPTCHA code entered..
        cg.code='AX23ji';
        cg.charStr1New='A.jpg';
        cg.charStr2New='X.jpg';
        cg.charStr3New='X12.jpg';
        cg.charStr4New='X13.jpg';
        cg.charStr5New='X1j.jpg';
        cg.charStr6New='X1i.jpg';
        cg.submitCaptcha1(cg.code);
        //Invalid CAPTCHA code entered..
        cg.charStr1New='A.jpg';
        cg.charStr2New='X.jpg';
        cg.charStr3New='X12.jpg';
        cg.charStr4New='X13.jpg';
        cg.charStr5New='X1j.jpg';
        cg.charStr6New='X1e.jpg';
        cg.code='';
        cg.submitCaptcha1(cg.code);
        ApexPages.Message[] captchaMsg1= ApexPages.getMessages();
        System.assertEquals('Please enter valid captcha code.',captchaMsg1[0].getDetail());
        cg.code='AX23jE';
        cg.submitCaptcha1(cg.code);
        ApexPages.Message[] captchaMsg2= ApexPages.getMessages();       
        System.assertEquals('Please check the captcha entered!',captchaMsg2[1].getDetail());        
        Test.stopTest();
    }
}