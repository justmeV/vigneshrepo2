// Class           : AMEXUtilityController
// Description     : Class is used as an utility class to provide various details related to 
//                   recordTypes
//                   
// Author          : V. Mittal
// Created Date    : Sep 09, 2011
//-------------------------------------------------------------------------------
// Modified Date        Modified By         Description
//-------------------------------------------------------------------------------
// 
//-------------------------------------------------------------------------------
public class AMEXUtilityController {
    // Map to store all the record Types
    private static Map<Id, RecordType> recordTypeMap = new Map<Id,RecordType>([select Id, Name, SobjectType from RecordType LIMIT 50000]);
    
    /* @param: rtName - Name of recordType
     * @param: objectType - ObjectType Name of RecordType
     * @return: recordType id found for the given recordType name and sobjectType
     */
    public static String getRecordTypeId(String rtName, String objectType) {
        for(Id rtId : recordTypeMap.keySet()) {
            RecordType rt = recordTypeMap.get(rtId);
            if((rt.Name).equalsIgnoreCase(rtName) && rt.SobjectType.equalsIgnoreCase(objectType)) {
                return (String)rtId;
            }
        }
        
        return null;
    }
    
    /* @param: rtId - Id of recordType to get RecordType's name
     * @return: recordType Name found for the given recordType Id
     */
    public static String getRecordTypeName(Id rtId) {
        if(recordTypeMap.containsKey(rtId)) {
            return recordTypeMap.get(rtId).Name;
        }
        
        return null;
    }
    
    static Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
    static Map<String,String> objKeyPrefixMap = new Map<String,String>{};
    static {
        Set<String> keyPrefixSet = gd.keySet();
        for(String sObj : keyPrefixSet){
           Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
           String tempName = r.getName();
           String tempPrefix = r.getKeyPrefix();
           System.debug('Processing Object['+tempName + '] with Prefix ['+ tempPrefix+']');
           objKeyPrefixMap.put(tempName,tempPrefix);
        }
    }
    
    /* @param: objectType - SobjectType
     * @return: Sobject KeyPrefix for given SobjectType
     */
    public static String getKeyPrefixByObjectType(String objectType) {
        return objKeyPrefixMap.get(objectType);
    }
}