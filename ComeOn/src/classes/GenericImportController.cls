public class GenericImportController {

    public String obj{get;set;}
    public Blob csvFileBody{get;set;}
    Public string csvAsString{get;set;}
    Public String[] csvfilelines{get;set;}
    //Public String[] inputvalues{get;set;}
    Public List<account> sObjectList{get;set;}
    public  Set<String> fieldname{get;set;} 
    public List<Sobject> fieldstobeinserted {get;set;} 
    public String selectaction{get;set;}
    public List<SelectOption> fieldNames = new List<SelectOption>();
    public Map<String,String> fieldsinObj= new Map<String,String>();
    CSVParserUtil wrapper=new CSVParserUtil();
    transient public Database.SaveResult[] srList; 
    transient public Database.UpsertResult[] urList;
    transient public Database.DeleteResult[] dlList;
    public Integer successCount=0;
    public Integer failCount=0;
    public string errors='';
    public Integer errorCount=0;
    public Integer recordCount=0;    
    
    public GenericImportController()
    {
        csvfilelines = new String[]{};
        sObjectList = New List<sObject>(); 
        fieldname = new Set<String>();
    }
    
    public List<SelectOption> getSelectActions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Insert','Insert'));
        options.add(new SelectOption('Update','Update'));
        options.add(new SelectOption('Delete','Delete'));
        return options;
    }

    public List<selectOption> getObjectName()
    {
        //List<Schema.SobjectType> objects=Schema.getGlobalDescribe().Values();
        List<SelectOption> options = new List<SelectOption>();
   
        for(Schema.SObjectType f : ProcessInstance.TargetObjectId.getDescribe().getReferenceTo())
        {
            if(!f.getDescribe().CustomSetting)  
            {  
                options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getLabel()));
            }
        }
        return options;
    }
    Map<String, Schema.SObjectField> fieldMap2 = new Map<String, Schema.SObjectField>();
    public Map<String,String> getObjectFields() 
    {
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType ObjectSchema = schemaMap.get(obj);
        Map<String, Schema.SObjectField> fieldMap = ObjectSchema.getDescribe().fields.getMap();        
        fieldMap2 = ObjectSchema.getDescribe().fields.getMap();
        for (String fieldN: fieldMap.keySet()) 
        {  
            Schema.Sobjectfield field = fieldMap.get(fieldN);
            Schema.Describefieldresult fieldDesc = field.getDescribe();
            if( fieldDesc.isUpdateable()==true && fieldDesc.isCalculated()==false)
            {
                fieldNames.add(new SelectOption(fieldDesc.getLabel(),fieldDesc.getName()));
            }
        }
        
        for(SelectOption s:fieldNames)
        {
            //fieldName.add(String.valueOf(s.getlabel()));
            fieldsinObj.put(s.getValue().tolowercase(),s.getlabel().tolowercase());
        }
        return fieldsinObj;
    }
         
    
    Public pagereference readcsvFile() 
    {
        if(obj != null)
        {
            if(selectaction != null)
            {
                if(csvAsString != null)
                {
                     Integer indexvalue=csvAsString.lastindexof('.'); 
                     if(csvAsString.substring((indexvalue+1),(indexvalue+4)).equalsignorecase('csv'))
                     {
                     List<List<String>> valuefromCSVparser=new List<List<String>>();
                     List<String> actualRecs = new List<String>();
                     List<String> fieldvalue =  new List<String>();
                     List<String> firstRowValues;
                     Integer m=0;
                     Integer n=0;
                     Boolean exist=true;
                     String errorColumn='';
                        try{
                            csvAsString = csvFileBody.toString();
                            csvfilelines = csvAsString.split('\n');
                            valuefromCSVparser=wrapper.parseCSV(csvAsString); 

                                for(String st:valuefromCSVparser[m]) 
                                {                                  
                                    fieldvalue.add(st); 
                                }

                                fieldsinObj = getObjectFields();
                                
                                string[] csvRecordData = new string[]{};
                                fieldstobeinserted = new List<Sobject>();
                                errorColumn='';
                                String msg='' ;
                                for(Integer i=1;i<valuefromCSVparser.size();i++)
                                {                    
                                    //csvRecordData = csvfilelines[i].split(',');
                                    csvRecordData = valuefromCSVparser[i];                   
                                    Integer count=0;
                                    sObject ac = Schema.getGlobalDescribe().get(obj).newSObject(); 
                                    system.debug('mullai'+i);                                   
                                    for(String s :csvRecordData)
                                    {    
                                                                        
                                        for(Integer j=0;j<=fieldvalue.size()-1;j++)
                                        {                                            
                                         
                                            system.debug('fieldvalue[j].trim()!!!!!'+fieldvalue[j].tolowercase().trim());
                                            if(count == j)
                                            {  
                                                system.debug('exist?!!!!!'+(fieldsinObj.keySet().contains(fieldvalue[j].tolowercase().trim())));
                                                if(fieldsinObj.keySet().contains(fieldvalue[j].tolowercase().trim()))
                                                { 
                                                    //It provides to get the object fields data type.
                                                     Schema.DisplayType fielddataType = fieldMap2.get(fieldsinObj.get(fieldvalue[j].tolowercase().trim())).getDescribe().getType();
                                                   
                                                   try{  
                                                    if(fielddataType == Schema.DisplayType.Double || fielddataType == Schema.DisplayType.Percent || fielddataType == Schema.DisplayType.Currency || fielddataType == Schema.DisplayType.Integer)
                                                    {
                                                        ac.put(fieldsinObj.get(fieldvalue[j].tolowercase().trim()),Integer.valueof(s.trim()));                                    
                                                        
                                                    }
                                                    else if(fielddataType == Schema.DisplayType.Date)
                                                    {
                                                        
                                                        s=s.trim();
                                                        /*String[] myDateOnly = s.split(' ');
                                                        String[] strDate = myDateOnly[0].split('/');
                                                        Integer myIntDate = integer.valueOf(strDate[1]);
                                                        Integer myIntMonth = integer.valueOf(strDate[0]);
                                                        Integer myIntYear = integer.valueOf(strDate[2]);
                                                        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);*/
                                                        ac.put(fieldsinObj.get(fieldvalue[j].tolowercase().trim()),DateParser.parseDate_actual(s));                                    
                                                    }
                                                    else if(fielddataType == Schema.DisplayType.DateTime)
                                                    {
                                                        //DateTime datetimevalues=DateParser.parseDateTime(s.trim());
                                                        Datetime dt = DateTime.parse(s.trim());
                                                        ac.put(fieldsinObj.get(fieldvalue[j].tolowercase().trim()),dt);
                                                    }
                                                    else if(fielddataType == Schema.DisplayType.Boolean)
                                                    {
                                                        Boolean value = Boolean.valueOf(s);
                                                        ac.put(fieldsinObj.get(fieldvalue[j].tolowercase().trim()),value);
                                                    }
                                                    else
                                                    {
                                                        ac.put(fieldsinObj.get(fieldvalue[j].tolowercase().trim()),s);
                                                    }
                                                    }
                                                    catch(Exception e)
                                                    {
                                                         String errField = fieldvalue[j].trim();
                                                         errField = errField.substring(0,1).toUpperCase()+errField.substring(1,errField.length());
                                                         msg += e.getmessage()+'  entered  in  the '+'"'+errField +'"'+'  column  of  Record  '+i+'.'+'<br/> ';     
                                                         
                                                         
                                                    }   
                                                    count++;
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    if(fieldvalue[j].tolowercase().trim() == 'id')
                                                    {
                                                        ac.put('id',s.trim());
                                                    }
                                                    else
                                                    {
                                                        if(i == 1)
                                                        {
                                                            errorColumn+='The column '+ fieldvalue[j].tolowercase().trim()+' doesnt exist for the object. <br></br>';
                                                            system.debug('error col!!!!!'+errorcolumn);
                                                        }
                                                    }
                                                    count++;
                                                }
                                                break;
                                            }  
                                                                       
                                        }  
                                        
                                   }      
                                                          
                                   fieldstobeinserted.add(ac);
                                }
                                if(msg!=null && msg!='')
                                {
                                    ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.Error,+msg);     
                                    ApexPages.addMessage(mess);
                                    return null;
                                }    
                                system.debug('b4 dml error col is!!!!'+errorcolumn);
                                system.debug('num of records for DML is-!!!!!'+fieldstobeinserted.size());
                                if(fieldstobeinserted.size() > 0)
                                {
                                    if(errorColumn == null || errorColumn == '')
                                    {
                                        if(selectaction == 'Insert')
                                        srList= Database.insert(fieldstobeinserted,false);
                                        if(selectaction == 'Update')
                                        srList= Database.update(fieldstobeinserted,false);
                                        if(selectaction == 'Delete')
                                        dlList= Database.delete(fieldstobeinserted,false);
                                        
                                        errors='';
                                        if(selectaction != 'Delete')
                                        {
                                            for (Database.SaveResult sr : srList) 
                                            {
                                                recordCount++;
                                                if (sr.isSuccess()) 
                                                {  
                                                    successCount++;
                                                }
                                                else
                                                {  
                                                errorCount = 0;                                 
                                                 // Operation failed, getting all the errors.
                                                  for(Database.Error err : sr.getErrors()) 
                                                   {
                                                       errorCount++;
                                                       //System.debug('The following error has occurred.');                   
                                                       //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                                       //System.debug('Fields that affected this error: ' + err.getFields());
                                                       errors += 'In Record '+recordCount+', error: '+err.getMessage()+'. '+'<br></br>'; //+errorCount+'.'
                                                   }          
                                                   failCount++;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            // Iterate through each returned result
                                            for(Database.DeleteResult dr : dlList) {
                                                recordCount++;
                                                if (dr.isSuccess()) {
                                                    // Operation was successful, so get the ID of the record that was processed
                                                    //System.debug('Successfully deleted '+obj+' with ID: ' + dr.getId());
                                                    successCount++;
                                                }
                                            else{
                                                errorCount = 0;
                                                    // Operation failed, so get all errors               
                                                    for(Database.Error err : dr.getErrors()) {
                                                    errorCount++;
                                                    //System.debug('The following error has occurred.');                   
                                                    //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                                    //System.debug('Fields that affected this error: ' + err.getFields());
                                                    errors += 'In Record '+recordCount+', error: '+err.getMessage()+'. '+'<br></br>'; //+errorCount+'.'
                                                    }
                                                    failCount++;
                                                }
                                            }
                                        }
                                        if(selectaction == 'Insert')
                                        {
                                            if(successCount == fieldstobeinserted.size())
                                            {    
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Inserted '+successCount+' record(s).');     
                                                ApexPages.addMessage(mess);
                                            }
                                            else if(failCount != 0 && failCount != fieldstobeinserted.size())
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Inserted '+successCount+' record(s). '+failCount+' record(s) failed. <br></br>'+errors);     
                                                ApexPages.addMessage(mess);                                
                                            }
                                            else
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Upload failed.'+'<br></br>'+ errors+'<br></br>'+'If no errors, please refresh the page and try again! If the error still persists, please contact the administrator.');     
                                                ApexPages.addMessage(mess);
                                            }
                                        }
                                        else if(selectaction == 'Update')
                                        {
                                            if(successCount == fieldstobeinserted.size())
                                            {    
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Updated '+successCount+' record(s).');     
                                                ApexPages.addMessage(mess);
                                            }
                                            else if(failCount != 0 && failCount != fieldstobeinserted.size())
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Updated '+successCount+' record(s). '+failCount+' record(s) failed. <br></br>'+errors);     
                                                ApexPages.addMessage(mess);                                
                                            }
                                            else
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Upload failed.'+'<br></br>'+ errors+'<br></br>'+'If no errors, please refresh the page and try again! If the error still persists, please contact the administrator.');     
                                                ApexPages.addMessage(mess);
                                            }
                                        }
                                        else
                                        {
                                            if(successCount == fieldstobeinserted.size())
                                            {    
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Deleted '+successCount+' record(s).');     
                                                ApexPages.addMessage(mess);
                                            }
                                            else if(failCount != 0 && failCount != fieldstobeinserted.size())
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.INFO,'Successfully Deleted '+successCount+' record(s). '+failCount+' record(s) failed. <br></br>'+errors);     
                                                ApexPages.addMessage(mess);                                
                                            }
                                            else
                                            {
                                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Deletion failed.'+'<br></br>'+ errors+'<br></br>'+'If no errors, please refresh the page and try again! If the error still persists, please contact the administrator.');     
                                                ApexPages.addMessage(mess);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,errorcolumn+'<br></br>Please check the column headers and upload again.');     
                                        ApexPages.addMessage(mess);
                                    }
                                }
                                else
                                {
                                    ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'No records to insert/update/delete!');     
                                    ApexPages.addMessage(mess);
                                }
                                fieldvalue = null;
                            }
                            catch(Exception e)
                            {
                                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
                                ApexPages.addMessage(mess);
                                fieldvalue = null;
                            }
                    } 
                    else
                    {
                        ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'The file should be of CSV type!');     
                        ApexPages.addMessage(mess);
                    }
                }
                else
                {
                    ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Please select a file to upload!');     
                    ApexPages.addMessage(mess);
                }
            }
            else
            {
                ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Please select an action!');     
                ApexPages.addMessage(mess);
            }
        }
        else
        {
            ApexPages.Message mess= new ApexPages.Message(ApexPages.severity.ERROR,'Please select a object!');     
            ApexPages.addMessage(mess);
        }
        fieldstobeinserted = null;
        successCount = 0;
        failCount = 0;
        errorCount=0;
        recordCount=0;
        errors='';
        return null;
    }
}