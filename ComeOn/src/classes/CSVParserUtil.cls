public class CSVParserUtil
{

 /* public String replaceConsecDQuotes(String content)
  {
     List<String> charArray=new List<String>();
     String newContent='';
     String[] alldatawithoutDQ=content.split('""');
     for(Integer x=1;x<alldatawithoutDQ.size();x++)
     {
        newContent=newContent+'#RCDDQUOTES#'+alldatawithoutDQ[x];
     }
       
       /*for(Integer x=0;x<content.length();x++)
       {
          System.debug('#rc at'+x+newContent);
          if(content.substring(x, x+1)=='""')
          {
             newContent=newContent+'#RCDDQUOTES#';
             
             x++;
          }
          else
          {
             newContent=newContent+content.substring(x, x);
          }
       }
       
      
       return newContent;
  } 
  */
   public List<List<String>> parseCSV(String content)
   {
       List<List<String>> returnParsed=new List<List<String>>();
       //System.debug('#rc parsing Content'+content);
       content=content.replaceAll('""','#RCDDQUOTES#');
       //content=replaceConsecDQuotes(content);
       System.debug('#rc dQ removed'+content);
       content=replaceAllCommasAndNewLines(content);
       String[] allRows=content.split('\n');
       for(Integer x=0;x<allRows.size();x++)
       {
         List<String> theRow=new List<String>();
         String[] fields=allRows[x].split(',',-2);
           for(Integer y=0;y<fields.size();y++)
           {
              String field=fields[y].replaceAll('"','');
              field=field.replaceAll('#RCCOMMA#',',');
               field=field.replaceAll('#RCNEWLINE#','\n');
               field=field.replaceAll('#RCDDQUOTES#','"');
              theRow.add(field);
            }  
            returnParsed.add(theRow);
       }
      return returnParsed; 
   }
   
   public String replaceAllCommasAndNewLines(String content)
   {
      String[] quotesSplit=content.split('"',-2);
       if(quotesSplit.size()>1)
       {
          for(Integer x=1;x<quotesSplit.size();x=x+2)
          {
             quotesSplit[x]= quotesSplit[x].replaceAll(',','#RCCOMMA#');
             quotesSplit[x]= quotesSplit[x].replaceAll('\n','#RCNEWLINE#');
          }
          
          String newContent='';
          for(Integer x=0;x<quotesSplit.size();x++)
          {
             newContent=newContent+quotesSplit[x];
          }
          return newContent;
       }
      else
      {
          return content;
      }
   }
   
   //overriding for PLC mass upload
   public List<List<String>> parseCSVPLCR(String content)
   {
       List<List<String>> returnParsed=new List<List<String>>();
       String[] allRows=content.split('\n');
       for(Integer x=1;x<allRows.size();x++)
       {
         String[] fields=allRows[x].split(',');
         returnParsed.add(fields);
       }
      return returnParsed; 
   }
   
}